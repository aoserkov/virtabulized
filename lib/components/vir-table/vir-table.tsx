import memoizeOne from "memoize-one";
import * as React from "react";
import { CellDimensions, TableBacking } from "../../backing";
import { CellGeometry } from "../../backing/table-backing";
import { Stuck } from "../../ranges";
import { VirCell } from "../vir-cell";
import { VirTableProps } from "./vir-table-props";
import { RenderArea, VirTableState } from "./vir-table-state";
import { fixedTable, leftTable, topTable, virTable } from "./vir-table.css";

const MINIMUM_OVERSCAN_FACTOR: number = 0.5;
const MAXIMUM_OVERSCAN_FACTOR: number = 1.5;

interface CellPosition {
  row: number;
  col: number;
}

enum KEYS {
  UP_ARROW = 38,
  DOWN_ARROW = 40,
  LEFT_ARROW = 37,
  RIGHT_ARROW = 39,
  ENTER = 13,
  TAB = 9
}

function moveBound(lowerBound: number, upperBound: number, currentValue: number): number {
  if (currentValue < lowerBound) {
    return upperBound;
  } else if (currentValue > upperBound) {
    return lowerBound;
  } else {
    return currentValue;
  }
}

export class VirTable extends React.Component<VirTableProps, VirTableState> {
  private getBodyDimensions: (width: number, height: number) => React.CSSProperties = memoizeOne(
    (width: number, height: number): React.CSSProperties => ({
      height: `${height}px`,
      width: `${width}px`
    })
  );

  private rootContainer: HTMLDivElement | null;
  private scrollTopContainer: HTMLDivElement | null;
  private scrollLeftContainer: HTMLDivElement | null;
  private scrollFixedContainer: HTMLDivElement | null;

  constructor(props: VirTableProps) {
    super(props);
    this.handleRootRef = this.handleRootRef.bind(this);
    this.updateArea = this.updateArea.bind(this);
    this.updateScrollTop = this.updateScrollTop.bind(this);
    this.updateScrollLeft = this.updateScrollLeft.bind(this);
    this.updateScrollFixed = this.updateScrollFixed.bind(this);
    this.scrollToCarriage = this.scrollToCarriage.bind(this);
    this.shiftCarriage = this.shiftCarriage.bind(this);
    this.moveCarriageToMousePosition = this.moveCarriageToMousePosition.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.handleClick = this.handleClick.bind(this);

    const backing: TableBacking = TableBacking.createBacking(props.model);
    this.state = {
      backing,
      carriagePosition: {
        row: 0,
        col: 0
      },
      renderArea: {
        fromWidth: 0,
        toWidth: 0,
        fromHeight: 0,
        toHeight: 0
      }
    };
    this.scrollTopContainer = null;
    this.scrollLeftContainer = null;
    this.scrollFixedContainer = null;
  }

  updateScrollTop(el: HTMLDivElement): void {
    this.scrollTopContainer = el;
  }

  updateScrollLeft(el: HTMLDivElement): void {
    this.scrollLeftContainer = el;
  }

  updateScrollFixed(el: HTMLDivElement): void {
    this.scrollFixedContainer = el;
  }

  shouldComponentUpdate(_: Readonly<VirTableProps>, nextState: Readonly<VirTableState>): boolean {
    return nextState !== this.state;
  }

  private updateArea(): void {
    if (this.rootContainer) {
      const ref: HTMLDivElement = this.rootContainer;
      const { backing, renderArea }: VirTableState = this.state;

      const areaWidth: number = ref.clientWidth;
      const areaHeight: number = ref.clientHeight;
      const scrollLeft: number = ref.scrollLeft;
      const scrollTop: number = ref.scrollTop;

      if (this.scrollTopContainer) {
        this.scrollTopContainer.style.transform = `translateY(${scrollTop}px)`;
      }

      if (this.scrollLeftContainer) {
        this.scrollLeftContainer.style.transform = `translateX(${scrollLeft}px)`;
      }

      if (this.scrollFixedContainer) {
        this.scrollFixedContainer.style.transform = `translate(${scrollLeft}px,${scrollTop}px)`;
      }

      const fromWidth: number = moveBound(
        Math.max(0, scrollLeft - areaWidth * MAXIMUM_OVERSCAN_FACTOR),
        Math.max(0, scrollLeft - areaWidth * MINIMUM_OVERSCAN_FACTOR),
        renderArea.fromWidth
      );
      const toWidth: number = moveBound(
        Math.min(backing.width, scrollLeft + areaWidth + areaWidth * MINIMUM_OVERSCAN_FACTOR),
        Math.min(backing.width, scrollLeft + areaWidth + areaWidth * MAXIMUM_OVERSCAN_FACTOR),
        renderArea.toWidth
      );
      const fromHeight: number = moveBound(
        Math.max(0, scrollTop - areaHeight * MAXIMUM_OVERSCAN_FACTOR),
        Math.max(0, scrollTop - areaHeight * MINIMUM_OVERSCAN_FACTOR),
        renderArea.fromHeight
      );
      const toHeight: number = moveBound(
        Math.min(backing.height, scrollTop + areaHeight + areaHeight * MINIMUM_OVERSCAN_FACTOR),
        Math.min(backing.height, scrollTop + areaHeight + areaHeight * MAXIMUM_OVERSCAN_FACTOR),
        renderArea.toHeight
      );

      if (
        fromWidth !== renderArea.fromWidth ||
        toWidth !== renderArea.toWidth ||
        fromHeight !== renderArea.fromHeight ||
        toHeight !== renderArea.toHeight
      ) {
        this.setState((state: VirTableState) => ({
          ...state,
          renderArea: { fromWidth, toWidth, fromHeight, toHeight }
        }));
      }
    }
  }

  handleRootRef(ref: HTMLDivElement): void {
    this.rootContainer = ref;
    this.updateArea();
  }

  private getAvailableCell(state: VirTableState, colOffset: number, rowOffset: number): CellPosition {
    const { carriagePosition } = state;

    const position: CellPosition = {
      col: carriagePosition.col + colOffset,
      row: carriagePosition.row + rowOffset
    };
    while (
      position.row < state.backing.getRowOffsets().length &&
      position.row >= 0 &&
      (position.col < state.backing.getColumnOffsets().length && position.col >= 0)
    ) {
      if (state.backing.getCellDimensions(position.col, position.row).selectable) {
        return position;
      } else {
        position.col += colOffset ? Math.sign(colOffset) : 0;
        position.row += rowOffset ? Math.sign(rowOffset) : 0;
      }
    }

    return state.carriagePosition;
  }

  private getTopContainerHeight(): number {
    if (this.scrollTopContainer && this.scrollTopContainer.lastChild) {
      const { lastChild } = this.scrollTopContainer;

      // @ts-ignore
      return lastChild.offsetTop + lastChild.clientHeight;
    } else {
      return 0;
    }
  }

  private getLeftContainerWidth(): number {
    if (this.scrollLeftContainer && this.scrollLeftContainer.lastChild) {
      const { lastChild } = this.scrollLeftContainer;

      // @ts-ignore
      return lastChild.offsetLeft + lastChild.clientWidth;
    } else {
      return 0;
    }
  }

  private getFixedContainerWidth(): number {
    if (this.scrollFixedContainer && this.scrollFixedContainer.lastChild) {
      const { lastChild } = this.scrollFixedContainer;

      // @ts-ignore
      return lastChild.offsetLeft + lastChild.clientWidth;
    } else {
      return 0;
    }
  }

  private getFixedContainerHeight(): number {
    if (this.scrollFixedContainer && this.scrollFixedContainer.lastChild) {
      const { lastChild } = this.scrollFixedContainer;

      // @ts-ignore
      return lastChild.offsetTop + lastChild.clientHeight;
    } else {
      return 0;
    }
  }

  private scrollToCarriage(): void {
    const { carriagePosition } = this.state;

    if (this.rootContainer) {
      const cellDimensions: CellDimensions = this.state.backing.getCellDimensions(
        carriagePosition.col,
        carriagePosition.row
      );

      if (!cellDimensions.stuck) {
        const cellPosition: CellGeometry = {
          ...cellDimensions.offset,
          right: cellDimensions.offset.left + cellDimensions.size.width,
          bottom: cellDimensions.offset.top + cellDimensions.size.height
        };

        const { rootContainer } = this;

        const leftOffset: number = this.getLeftContainerWidth();
        if (cellPosition.right > rootContainer.scrollLeft + rootContainer.clientWidth) {
          rootContainer.scrollLeft = cellPosition.right - rootContainer.clientWidth;
        } else if (cellPosition.left < rootContainer.scrollLeft + leftOffset) {
          this.rootContainer.scrollLeft = cellPosition.left - leftOffset;
        }

        const topOffset: number = this.getTopContainerHeight();
        if (cellPosition.bottom > rootContainer.scrollTop + rootContainer.clientHeight) {
          rootContainer.scrollTop = cellPosition.bottom - rootContainer.clientHeight;
        } else if (cellPosition.top < rootContainer.scrollTop + topOffset) {
          rootContainer.scrollTop = cellPosition.top - topOffset;
        }

        this.updateArea();
      }
    }
  }

  private shiftCarriage(col: number, row: number): void {
    this.setState((state: VirTableState) => ({
      ...state,
      carriagePosition: this.getAvailableCell(state, col, row)
    }));
  }

  private moveCarriageToMousePosition(x: number, y: number): void {
    const { backing } = this.state;
    if (this.rootContainer) {
      const { rootContainer } = this;
      const widthOffset: number =
        x + (x > this.getLeftContainerWidth() && x > this.getFixedContainerWidth() ? rootContainer.scrollLeft : 0);
      const heightOffset: number =
        y + (y > this.getTopContainerHeight() && y > this.getFixedContainerHeight() ? rootContainer.scrollTop : 0);

      const col: number = backing.getColumnAtOffset(widthOffset);
      const row: number = backing.getRowAtOffset(heightOffset);
      if (this.state.backing.getCellDimensions(col, row).selectable) {
        this.setState((state: VirTableState) => ({ ...state, carriagePosition: { row, col } }));
      }
    }
  }

  private handleKeyDown(event: React.KeyboardEvent<HTMLDivElement>): void {
    switch (event.which) {
      case KEYS.LEFT_ARROW:
        event.preventDefault();
        this.shiftCarriage(-1, 0);
        break;
      case KEYS.RIGHT_ARROW:
      case KEYS.TAB:
        event.preventDefault();
        this.shiftCarriage(1, 0);
        break;
      case KEYS.UP_ARROW:
        event.preventDefault();
        this.shiftCarriage(0, -1);
        break;
      case KEYS.ENTER:
      case KEYS.DOWN_ARROW:
        event.preventDefault();
        this.shiftCarriage(0, 1);
        break;
    }
  }

  private handleClick(event: React.MouseEvent<HTMLDivElement>): void {
    if (this.rootContainer) {
      const { left, top } = this.rootContainer.getBoundingClientRect();
      const { scrollLeft, scrollTop } = this.rootContainer;
      const xOffset: number = event.clientX - left + scrollLeft;
      const yOffset: number = event.clientY - top + scrollTop;

      if (xOffset < this.state.backing.width && yOffset < this.state.backing.height) {
        this.moveCarriageToMousePosition(event.clientX - left, event.clientY - top);
        event.preventDefault();
      }
    }
  }

  componentDidUpdate(_: VirTableProps, prevState: VirTableState): void {
    if (
      prevState.carriagePosition.row !== this.state.carriagePosition.row ||
      prevState.carriagePosition.col !== this.state.carriagePosition.col
    ) {
      this.scrollToCarriage();
    }

    if (prevState.backing !== this.state.backing && this.rootContainer) {
      this.updateArea();
    }
  }

  componentDidMount(): void {
    if (this.rootContainer) {
      this.updateArea();
    }
  }

  render(): React.ReactElement {
    const { style, className }: VirTableProps = this.props;
    const { backing, renderArea, carriagePosition }: VirTableState = this.state;

    const allCells: CellDimensions[] = backing.getCells(
      renderArea.fromWidth,
      renderArea.toWidth,
      renderArea.fromHeight,
      renderArea.toHeight
    );
    const leftCells: React.ReactElement[] = [];
    const topCells: React.ReactElement[] = [];
    const fixedCells: React.ReactElement[] = [];
    const otherCells: React.ReactElement[] = [];

    for (const cell of allCells) {
      const inCarriageRow: boolean = cell.row === carriagePosition.row;
      const inCarriageColumn: boolean = cell.col === carriagePosition.col;

      const cellRender: React.ReactElement = (
        <VirCell
          top={cell.offset.top}
          left={cell.offset.left}
          key={`${cell.col}/${cell.row}`}
          width={cell.size.width}
          height={cell.size.height}
          renderer={backing.getCellRenderer(cell.col, cell.row)}
          selection={{
            inCarriageRow,
            inCarriageColumn,
            underCarriage: inCarriageColumn && inCarriageRow,
            selected: false
          }}
        />
      );

      if (!!cell.stuck && cell.stuck === Stuck.LEFT) {
        leftCells.push(cellRender);
      } else if (!!cell.stuck && cell.stuck === Stuck.TOP) {
        topCells.push(cellRender);
      } else if (!!cell.stuck && cell.stuck === Stuck.FIXED) {
        fixedCells.push(cellRender);
      } else {
        otherCells.push(cellRender);
      }
    }

    return (
      <div
        tabIndex={0}
        onKeyDown={this.handleKeyDown}
        onClick={this.handleClick}
        ref={this.handleRootRef}
        onScroll={this.updateArea}
        style={style}
        className={`${virTable} ${className || ""}`}
      >
        {!!renderArea.toWidth && (
          <div style={this.getBodyDimensions(backing.width, backing.height)}>
            {!!leftCells.length && (
              <div ref={this.updateScrollLeft} className={`${leftTable} ${className || ""}`}>
                {leftCells}
              </div>
            )}

            {!!topCells.length && (
              <div ref={this.updateScrollTop} className={`${topTable} ${className || ""}`}>
                {topCells}
              </div>
            )}

            {!!fixedCells.length && (
              <div ref={this.updateScrollFixed} className={`${fixedTable} ${className || ""}`}>
                {fixedCells}
              </div>
            )}

            {otherCells}
          </div>
        )}
      </div>
    );
  }

  static getDerivedStateFromProps(props: VirTableProps, state: VirTableState): VirTableState {
    const backing: TableBacking = state.backing.setModel(props.model);
    const renderArea: RenderArea = state.renderArea;

    if (backing === state.backing) {
      return state;
    }

    if (renderArea.toHeight > backing.height) {
      renderArea.fromHeight = Math.max(0, backing.height + renderArea.fromHeight - renderArea.toHeight);
      renderArea.toHeight = backing.height;
    }

    if (renderArea.toWidth > backing.width) {
      renderArea.fromWidth = Math.max(0, backing.width + renderArea.fromWidth - renderArea.toWidth);
      renderArea.toWidth = backing.width;
    }

    return {
      ...state,
      renderArea,
      backing
    };
  }
}
