(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('memoize-one'), require('react')) :
  typeof define === 'function' && define.amd ? define(['exports', 'memoize-one', 'react'], factory) :
  (global = global || self, factory(global.virtabulized = {}, global.memoizeOne, global.React));
}(this, function (exports, memoizeOne, React) { 'use strict';

  memoizeOne = memoizeOne && memoizeOne.hasOwnProperty('default') ? memoizeOne['default'] : memoizeOne;

  (function (Stuck) {
      Stuck["TOP"] = "top";
      Stuck["LEFT"] = "left";
      Stuck["FIXED"] = "fixed";
  })(exports.Stuck || (exports.Stuck = {}));

  /*! *****************************************************************************
  Copyright (c) Microsoft Corporation. All rights reserved.
  Licensed under the Apache License, Version 2.0 (the "License"); you may not use
  this file except in compliance with the License. You may obtain a copy of the
  License at http://www.apache.org/licenses/LICENSE-2.0

  THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
  WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
  MERCHANTABLITY OR NON-INFRINGEMENT.

  See the Apache Version 2.0 License for specific language governing permissions
  and limitations under the License.
  ***************************************************************************** */
  /* global Reflect, Promise */

  var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf ||
          ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
          function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
      return extendStatics(d, b);
  };

  function __extends(d, b) {
      extendStatics(d, b);
      function __() { this.constructor = d; }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
  }

  var __assign = function() {
      __assign = Object.assign || function __assign(t) {
          for (var s, i = 1, n = arguments.length; i < n; i++) {
              s = arguments[i];
              for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
          }
          return t;
      };
      return __assign.apply(this, arguments);
  };

  var zeroDimensions = { cols: 0, rows: 0 };
  function offsetConsumer(colOffset, rowOffset, consumer) {
      return function (result) {
          return consumer(__assign({}, result, { position: {
                  col: colOffset + result.position.col,
                  row: rowOffset + result.position.row
              } }));
      };
  }
  var RangeNode = /** @class */ (function () {
      function RangeNode(range, cache, dimensionConstraints) {
          this._range = range;
          this._dimensions = range.dimensions;
          this._cache = cache;
          if (this._dimensions && dimensionConstraints) {
              assertDimensionsFit(this._dimensions, dimensionConstraints, this._range.key);
          }
      }
      RangeNode.prototype.setRange = function (range, enclosingDimensions) {
          if (this._range !== range) {
              if (range.type === "cell") {
                  return new CellNode(range, enclosingDimensions);
              }
              else {
                  return new RangeNode(range, this._cache, enclosingDimensions);
              }
          }
          if (this._dimensions && enclosingDimensions) {
              assertDimensionsFit(this._dimensions, enclosingDimensions, this._range.key);
          }
          return this;
      };
      Object.defineProperty(RangeNode.prototype, "dimensions", {
          get: function () {
              if (!this._dimensions) {
                  this._dimensions = this.calculateDimensions();
              }
              return this._dimensions;
          },
          enumerable: true,
          configurable: true
      });
      Object.defineProperty(RangeNode.prototype, "stuck", {
          get: function () {
              return this._range.stuck;
          },
          enumerable: true,
          configurable: true
      });
      Object.defineProperty(RangeNode.prototype, "selectable", {
          get: function () {
              return this._range.selectable;
          },
          enumerable: true,
          configurable: true
      });
      RangeNode.prototype.findCellsInRange = function (fromCol, toCol, fromRow, toRow, enclosingDimensions, consumer, stuck, selectable) {
          if (!this._order) {
              this.fillCache();
          }
          if (!this._order) {
              throw new Error("Unexpected state: order not set despite filling caches for " + this._range.key);
          }
          var offset = 0;
          switch (this._range.type) {
              case "horizontal":
                  for (var _i = 0, _a = this._order; _i < _a.length; _i++) {
                      var key = _a[_i];
                      if (offset >= toCol) {
                          break;
                      }
                      var node = this._cache[key];
                      var currentStuck = node.stuck;
                      var currentSelectable = node.selectable;
                      if (currentStuck !== undefined || fromCol - offset < node.dimensions.cols) {
                          var stop_1 = node.findCellsInRange(currentStuck !== undefined ? 0 : fromCol - offset, Math.min(toCol - offset, node.dimensions.cols), fromRow, Math.min(toRow, this.dimensions.rows), { cols: node.dimensions.cols, rows: this.dimensions.rows }, offsetConsumer(offset, 0, consumer), currentStuck || stuck, currentSelectable !== undefined ? currentSelectable : selectable);
                          if (stop_1) {
                              return stop_1;
                          }
                      }
                      offset += node.dimensions.cols;
                  }
                  if (offset < toCol) {
                      return pad(toCol, toRow, { cols: offset, rows: this.dimensions.rows }, enclosingDimensions, consumer);
                  }
                  return false;
              case "vertical":
              case "cellGroup":
                  for (var _b = 0, _c = this._order; _b < _c.length; _b++) {
                      var key = _c[_b];
                      if (offset >= toRow) {
                          break;
                      }
                      var node = this._cache[key];
                      var currentStuck = node.stuck;
                      var currentSelectable = node.selectable;
                      if (currentStuck !== undefined || fromRow - offset < node.dimensions.rows) {
                          var stop_2 = node.findCellsInRange(fromCol, Math.min(toCol, this.dimensions.cols), currentStuck !== undefined ? 0 : fromRow - offset, Math.min(toRow - offset, node.dimensions.rows), { cols: this.dimensions.cols, rows: node.dimensions.rows }, offsetConsumer(0, offset, consumer), currentStuck || stuck, currentSelectable !== undefined ? currentSelectable : selectable);
                          if (stop_2) {
                              return stop_2;
                          }
                      }
                      offset += node.dimensions.rows;
                  }
                  if (offset < toRow) {
                      return pad(toCol, toRow, { cols: this.dimensions.cols, rows: offset }, enclosingDimensions, consumer);
                  }
                  return false;
              default:
                  throw new Error("Unexpected range type " + this._range.type);
          }
      };
      RangeNode.prototype.calculateDimensions = function () {
          var _this = this;
          if (!this._order) {
              this.fillCache();
          }
          if (!this._order) {
              throw new Error("Unexpected state: order not set despite filling caches for " + this._range.key);
          }
          var reducer;
          switch (this._range.type) {
              case "horizontal":
                  reducer = function (a, i) { return ({
                      cols: a.cols + i.cols,
                      rows: Math.max(a.rows, i.rows)
                  }); };
                  break;
              case "vertical":
              case "cellGroup":
                  reducer = function (a, i) { return ({
                      cols: Math.max(a.cols, i.cols),
                      rows: a.rows + i.rows
                  }); };
                  break;
              default:
                  throw new Error("Unexpected range type " + this._range.type);
          }
          return this._order
              .map(function (k) { return _this._cache[k]; })
              .map(function (n) { return n.dimensions; })
              .reduce(reducer, zeroDimensions);
      };
      RangeNode.prototype.fillCache = function () {
          var children;
          switch (this._range.type) {
              case "horizontal":
              case "vertical":
              case "cellGroup":
                  children = this._range.getChildren();
                  break;
              default:
                  throw new Error("Unexpected range type " + this._range.type);
          }
          var order = [];
          var cache = {};
          var constraint = this._dimensions;
          for (var _i = 0, children_1 = children; _i < children_1.length; _i++) {
              var childRange = children_1[_i];
              var key = childRange.key;
              if (key === null || key === undefined) {
                  throw new Error("Undefined key in range with " + this._range.key);
              }
              order.push(key);
              var oldOne = this._cache ? this._cache[key] : undefined;
              if (key in cache) {
                  throw new Error("Duplicate key: " + key);
              }
              var childNode = void 0;
              if (oldOne) {
                  childNode = oldOne.setRange(childRange, constraint);
              }
              else if (childRange.type === "cell") {
                  childNode = new CellNode(childRange, constraint);
              }
              else {
                  childNode = new RangeNode(childRange, {}, constraint);
              }
              cache[key] = childNode;
              if (constraint && ((childNode instanceof RangeNode && childNode._dimensions) || childNode instanceof CellNode)) {
                  switch (this._range.type) {
                      case "horizontal":
                          constraint = {
                              rows: constraint.rows,
                              cols: constraint.cols - childNode.dimensions.cols
                          };
                          break;
                      case "vertical":
                      case "cellGroup":
                          constraint = {
                              rows: constraint.rows - childNode.dimensions.rows,
                              cols: constraint.cols
                          };
                          break;
                      default:
                          throw new Error("Unexpected range type"); // Unable to output type due to "never" deferred type
                  }
              }
          }
          this._order = order;
          this._cache = cache;
      };
      return RangeNode;
  }());

  var CellNode = /** @class */ (function () {
      function CellNode(cell, enclosingDimensions) {
          this._cell = cell;
          this._dimensions = cell.dimensions;
          if (enclosingDimensions) {
              assertDimensionsFit(this._dimensions, enclosingDimensions, this._cell.key);
          }
      }
      Object.defineProperty(CellNode.prototype, "dimensions", {
          get: function () {
              return this._dimensions;
          },
          enumerable: true,
          configurable: true
      });
      Object.defineProperty(CellNode.prototype, "stuck", {
          get: function () {
              return this._cell.stuck;
          },
          enumerable: true,
          configurable: true
      });
      Object.defineProperty(CellNode.prototype, "selectable", {
          get: function () {
              return this._cell.selectable;
          },
          enumerable: true,
          configurable: true
      });
      CellNode.prototype.findCellsInRange = function (fromCol, toCol, fromRow, toRow, enclosingDimensions, consumer, stuck, selectable) {
          if (this._dimensions.cols > fromCol && this._dimensions.rows > fromRow) {
              var stop_1 = consumer({
                  cell: this._cell,
                  stuck: this.stuck || stuck,
                  selectable: selectable,
                  dimensions: this._dimensions,
                  position: { col: 0, row: 0 }
              });
              if (stop_1) {
                  return stop_1;
              }
          }
          return pad(toCol, toRow, this._dimensions, enclosingDimensions, consumer);
      };
      CellNode.prototype.setRange = function (range, enclosingDimensions) {
          if (range === this._cell) {
              if (enclosingDimensions) {
                  assertDimensionsFit(this._dimensions, enclosingDimensions, this._cell.key);
              }
              return this;
          }
          if (range.type === "cell") {
              return new CellNode(range, enclosingDimensions);
          }
          else {
              return new RangeNode(range, {}, enclosingDimensions);
          }
      };
      return CellNode;
  }());

  function pad(toCol, toRow, innerDimensions, enclosingDimensions, consumer) {
      if (innerDimensions.cols < toCol) {
          var stop_1 = consumer({
              cell: null,
              position: {
                  col: innerDimensions.cols,
                  row: 0
              },
              dimensions: {
                  cols: enclosingDimensions.cols - innerDimensions.cols,
                  rows: innerDimensions.rows
              }
          });
          if (stop_1) {
              return stop_1;
          }
      }
      if (innerDimensions.rows < toRow) {
          var stop_2 = consumer({
              cell: null,
              position: {
                  col: 0,
                  row: innerDimensions.rows
              },
              dimensions: {
                  cols: enclosingDimensions.cols,
                  rows: enclosingDimensions.rows - innerDimensions.rows
              }
          });
          if (stop_2) {
              return stop_2;
          }
      }
      return false;
  }
  function assertDimensionsFit(dimensions, constraints, key) {
      if (constraints.cols < dimensions.cols || constraints.rows < dimensions.rows) {
          throw new Error("Inner content dimensions (" + dimensions.cols + "x" + dimensions.rows + ") " +
              ("for range with key " + key + " is greater than dimensions of parent ") +
              ("(" + constraints.cols + "x" + constraints.rows + ")"));
      }
  }
  function createNode(range, enclosingDimensions) {
      if (range.type === "cell") {
          return new CellNode(range, enclosingDimensions);
      }
      else {
          return new RangeNode(range, {}, enclosingDimensions);
      }
  }

  function accumulate(diffs) {
      var result = [];
      var acc = 0;
      for (var _i = 0, diffs_1 = diffs; _i < diffs_1.length; _i++) {
          var h = diffs_1[_i];
          acc += h;
          result.push(acc);
      }
      return result;
  }
  function findOffset(offsets, offset) {
      var h = offsets.length;
      var l = 0;
      if (offset < 0 || offset >= offsets[h - 1]) {
          throw new Error("Offset " + offset + " is out of bounds [0, " + offsets[h - 1] + ")");
      }
      while (l + 1 < h) {
          var p = Math.floor((l + h) / 2);
          if (offsets[p] < offset) {
              l = p;
          }
          else if (offsets[p] > offset) {
              h = p;
          }
          else {
              return p + 1;
          }
      }
      if (offsets[l] > offset) {
          return l;
      }
      else {
          return l + 1;
      }
  }
  var TableBacking = /** @class */ (function () {
      function TableBacking(model, rangeTree, colOffsets, rowOffsets) {
          this._model = model;
          this._rowOffsets = rowOffsets;
          this._columnOffsets = colOffsets;
          this._rangeTree = rangeTree;
      }
      Object.defineProperty(TableBacking.prototype, "width", {
          get: function () {
              return this._columnOffsets[this._columnOffsets.length - 1];
          },
          enumerable: true,
          configurable: true
      });
      Object.defineProperty(TableBacking.prototype, "height", {
          get: function () {
              return this._rowOffsets[this._rowOffsets.length - 1];
          },
          enumerable: true,
          configurable: true
      });
      TableBacking.prototype.getColumnAtOffset = function (widthOffset) {
          return findOffset(this._columnOffsets, widthOffset);
      };
      TableBacking.prototype.getRowAtOffset = function (heightOffset) {
          return findOffset(this._rowOffsets, heightOffset);
      };
      TableBacking.prototype.getRowOffsets = function () {
          return this._rowOffsets;
      };
      TableBacking.prototype.getColumnOffsets = function () {
          return this._columnOffsets;
      };
      TableBacking.prototype.checkRange = function (col, row) {
          if (row >= this._rowOffsets.length || row < 0) {
              throw new Error("Row should be greater than 0 and less than " + this._rowOffsets.length + ", was " + row);
          }
          if (col >= this._columnOffsets.length || col < 0) {
              throw new Error("Row should be greater than 0 and less than " + this._columnOffsets.length + ", was " + col);
          }
      };
      TableBacking.prototype.calculateCellDimensions = function (_a) {
          var position = _a.position, cell = _a.cell, dimensions = _a.dimensions, stuck = _a.stuck, selectable = _a.selectable;
          var col = position.col, row = position.row;
          var left = col === 0 ? 0 : this._columnOffsets[col - 1];
          var top = row === 0 ? 0 : this._rowOffsets[row - 1];
          var right = this._columnOffsets[col + dimensions.cols - 1];
          var bottom = this._rowOffsets[row + dimensions.rows - 1];
          return {
              selectable: selectable,
              col: col,
              row: row,
              offset: { left: left, top: top },
              size: {
                  width: right - left,
                  height: bottom - top
              },
              empty: !cell,
              stuck: stuck
          };
      };
      TableBacking.prototype.getCellDimensions = function (col, row) {
          this.checkRange(col, row);
          var result = null;
          this._rangeTree.findCellsInRange(col, col + 1, row, row + 1, { cols: this._columnOffsets.length, rows: this._rowOffsets.length }, function (r) {
              return r.position.col <= col &&
                  r.position.col + r.dimensions.cols > col &&
                  r.position.row <= row &&
                  r.position.row + r.dimensions.rows > row
                  ? !!(result = r)
                  : false;
          });
          if (!result) {
              throw new Error("Unexpected state for col " + col + ", row " + row);
          }
          return this.calculateCellDimensions(result);
      };
      TableBacking.prototype.getCellRenderer = function (col, row) {
          this.checkRange(col, row);
          var result = null;
          this._rangeTree.findCellsInRange(col, col + 1, row, row + 1, { cols: this._columnOffsets.length, rows: this._rowOffsets.length }, function (r) {
              return r.position.col <= col &&
                  r.position.col + r.dimensions.cols > col &&
                  r.position.row <= row &&
                  r.position.row + r.dimensions.rows > row
                  ? !!(result = r)
                  : false;
          });
          if (!result) {
              throw new Error("Unexpected state for col " + col + ", row " + row);
          }
          var cell = result.cell;
          if (cell) {
              return cell;
          }
          else {
              return {
                  render: function () {
                      return React.createElement(React.Fragment);
                  }
              };
          }
      };
      TableBacking.prototype.getCells = function (fromWidthOffset, toWidthOffset, fromHeightOffset, toHeightOffset) {
          var _this = this;
          if (fromWidthOffset === toWidthOffset || fromHeightOffset === toHeightOffset) {
              return [];
          }
          var fromCol = this.getColumnAtOffset(fromWidthOffset);
          var toCol = this.getColumnAtOffset(toWidthOffset - 1) + 1;
          var fromRow = this.getRowAtOffset(fromHeightOffset);
          var toRow = this.getRowAtOffset(toHeightOffset - 1) + 1;
          var result = [];
          this._rangeTree.findCellsInRange(fromCol, toCol, fromRow, toRow, { cols: this._columnOffsets.length, rows: this._rowOffsets.length }, function (r) { return !result.push(r) && false; });
          return result.map(function (r) { return _this.calculateCellDimensions(r); });
      };
      TableBacking.prototype.setModel = function (model) {
          var _this = this;
          if (model === this._model) {
              return this;
          }
          var colOffsets = accumulate(model.colWidths);
          var colMatches = colOffsets.length === this._columnOffsets.length &&
              colOffsets.reduce(function (r, o, i) { return r && o === _this._columnOffsets[i]; }, true);
          var rowOffsets = accumulate(model.rowHeights);
          var rowMatches = rowOffsets.length === this._rowOffsets.length &&
              rowOffsets.reduce(function (r, o, i) { return r && o === _this._rowOffsets[i]; }, true);
          var range = this._rangeTree.setRange(model.content, { cols: colOffsets.length, rows: rowOffsets.length });
          if (range !== this._rangeTree || !colMatches || !rowMatches) {
              return new TableBacking(model, range, colOffsets, rowOffsets);
          }
          else {
              return this;
          }
      };
      TableBacking.createBacking = function (model) {
          var colOffsets = accumulate(model.colWidths);
          var rowOffsets = accumulate(model.rowHeights);
          return new TableBacking(model, createNode(model.content, { cols: colOffsets.length, rows: rowOffsets.length }), colOffsets, rowOffsets);
      };
      return TableBacking;
  }());

  function styleInject(css, ref) {
    if ( ref === void 0 ) ref = {};
    var insertAt = ref.insertAt;

    if (!css || typeof document === 'undefined') { return; }

    var head = document.head || document.getElementsByTagName('head')[0];
    var style = document.createElement('style');
    style.type = 'text/css';

    if (insertAt === 'top') {
      if (head.firstChild) {
        head.insertBefore(style, head.firstChild);
      } else {
        head.appendChild(style);
      }
    } else {
      head.appendChild(style);
    }

    if (style.styleSheet) {
      style.styleSheet.cssText = css;
    } else {
      style.appendChild(document.createTextNode(css));
    }
  }

  var virCell = "vir-cell_virCell__3-gkN";
  var css = ".vir-cell_virCell__3-gkN {\n  position: absolute;\n}\n";
  styleInject(css);

  var VirCell = /** @class */ (function (_super) {
      __extends(VirCell, _super);
      function VirCell() {
          var _this = _super !== null && _super.apply(this, arguments) || this;
          _this.getStyle = memoizeOne(function (width, height, left, top) { return ({
              top: top + "px",
              left: left + "px",
              width: width + "px",
              height: height + "px"
          }); });
          return _this;
      }
      VirCell.prototype.render = function () {
          var _a = this.props, top = _a.top, left = _a.left, height = _a.height, width = _a.width, renderer = _a.renderer, selection = _a.selection;
          return (React.createElement("div", { style: this.getStyle(width, height, left, top), className: virCell }, renderer.render(selection)));
      };
      return VirCell;
  }(React.PureComponent));

  var virTable = "vir-table_virTable__2tNxB";
  var topTable = "vir-table_topTable__3YI6x";
  var leftTable = "vir-table_leftTable__qCBL9";
  var fixedTable = "vir-table_fixedTable__2u0iy";
  var css$1 = ".vir-table_virTable__2tNxB {\n    overflow: auto;\n    position: relative;\n}\n\n.vir-table_virTable__2tNxB > div {\n    position: absolute;\n}\n\n.vir-table_topTable__3YI6x {\n    position: absolute;\n    z-index: 1;\n}\n\n.vir-table_leftTable__qCBL9 {\n    position: absolute;\n    z-index: 2;\n}\n\n.vir-table_fixedTable__2u0iy {\n    position: absolute;\n    z-index: 3;\n}\n";
  styleInject(css$1);

  var MINIMUM_OVERSCAN_FACTOR = 0.5;
  var MAXIMUM_OVERSCAN_FACTOR = 1.5;
  var KEYS;
  (function (KEYS) {
      KEYS[KEYS["UP_ARROW"] = 38] = "UP_ARROW";
      KEYS[KEYS["DOWN_ARROW"] = 40] = "DOWN_ARROW";
      KEYS[KEYS["LEFT_ARROW"] = 37] = "LEFT_ARROW";
      KEYS[KEYS["RIGHT_ARROW"] = 39] = "RIGHT_ARROW";
      KEYS[KEYS["ENTER"] = 13] = "ENTER";
      KEYS[KEYS["TAB"] = 9] = "TAB";
  })(KEYS || (KEYS = {}));
  function moveBound(lowerBound, upperBound, currentValue) {
      if (currentValue < lowerBound) {
          return upperBound;
      }
      else if (currentValue > upperBound) {
          return lowerBound;
      }
      else {
          return currentValue;
      }
  }
  var VirTable = /** @class */ (function (_super) {
      __extends(VirTable, _super);
      function VirTable(props) {
          var _this = _super.call(this, props) || this;
          _this.getBodyDimensions = memoizeOne(function (width, height) { return ({
              height: height + "px",
              width: width + "px"
          }); });
          _this.handleRootRef = _this.handleRootRef.bind(_this);
          _this.updateArea = _this.updateArea.bind(_this);
          _this.updateScrollTop = _this.updateScrollTop.bind(_this);
          _this.updateScrollLeft = _this.updateScrollLeft.bind(_this);
          _this.updateScrollFixed = _this.updateScrollFixed.bind(_this);
          _this.scrollToCarriage = _this.scrollToCarriage.bind(_this);
          _this.shiftCarriage = _this.shiftCarriage.bind(_this);
          _this.moveCarriageToMousePosition = _this.moveCarriageToMousePosition.bind(_this);
          _this.handleKeyDown = _this.handleKeyDown.bind(_this);
          _this.handleClick = _this.handleClick.bind(_this);
          var backing = TableBacking.createBacking(props.model);
          _this.state = {
              backing: backing,
              carriagePosition: {
                  row: 0,
                  col: 0
              },
              renderArea: {
                  fromWidth: 0,
                  toWidth: 0,
                  fromHeight: 0,
                  toHeight: 0
              }
          };
          _this.scrollTopContainer = null;
          _this.scrollLeftContainer = null;
          _this.scrollFixedContainer = null;
          return _this;
      }
      VirTable.prototype.updateScrollTop = function (el) {
          this.scrollTopContainer = el;
      };
      VirTable.prototype.updateScrollLeft = function (el) {
          this.scrollLeftContainer = el;
      };
      VirTable.prototype.updateScrollFixed = function (el) {
          this.scrollFixedContainer = el;
      };
      VirTable.prototype.shouldComponentUpdate = function (_, nextState) {
          return nextState !== this.state;
      };
      VirTable.prototype.updateArea = function () {
          if (this.rootContainer) {
              var ref = this.rootContainer;
              var _a = this.state, backing = _a.backing, renderArea = _a.renderArea;
              var areaWidth = ref.clientWidth;
              var areaHeight = ref.clientHeight;
              var scrollLeft = ref.scrollLeft;
              var scrollTop = ref.scrollTop;
              if (this.scrollTopContainer) {
                  this.scrollTopContainer.style.transform = "translateY(" + scrollTop + "px)";
              }
              if (this.scrollLeftContainer) {
                  this.scrollLeftContainer.style.transform = "translateX(" + scrollLeft + "px)";
              }
              if (this.scrollFixedContainer) {
                  this.scrollFixedContainer.style.transform = "translate(" + scrollLeft + "px," + scrollTop + "px)";
              }
              var fromWidth_1 = moveBound(Math.max(0, scrollLeft - areaWidth * MAXIMUM_OVERSCAN_FACTOR), Math.max(0, scrollLeft - areaWidth * MINIMUM_OVERSCAN_FACTOR), renderArea.fromWidth);
              var toWidth_1 = moveBound(Math.min(backing.width, scrollLeft + areaWidth + areaWidth * MINIMUM_OVERSCAN_FACTOR), Math.min(backing.width, scrollLeft + areaWidth + areaWidth * MAXIMUM_OVERSCAN_FACTOR), renderArea.toWidth);
              var fromHeight_1 = moveBound(Math.max(0, scrollTop - areaHeight * MAXIMUM_OVERSCAN_FACTOR), Math.max(0, scrollTop - areaHeight * MINIMUM_OVERSCAN_FACTOR), renderArea.fromHeight);
              var toHeight_1 = moveBound(Math.min(backing.height, scrollTop + areaHeight + areaHeight * MINIMUM_OVERSCAN_FACTOR), Math.min(backing.height, scrollTop + areaHeight + areaHeight * MAXIMUM_OVERSCAN_FACTOR), renderArea.toHeight);
              if (fromWidth_1 !== renderArea.fromWidth ||
                  toWidth_1 !== renderArea.toWidth ||
                  fromHeight_1 !== renderArea.fromHeight ||
                  toHeight_1 !== renderArea.toHeight) {
                  this.setState(function (state) { return (__assign({}, state, { renderArea: { fromWidth: fromWidth_1, toWidth: toWidth_1, fromHeight: fromHeight_1, toHeight: toHeight_1 } })); });
              }
          }
      };
      VirTable.prototype.handleRootRef = function (ref) {
          this.rootContainer = ref;
          this.updateArea();
      };
      VirTable.prototype.getAvailableCell = function (state, colOffset, rowOffset) {
          var carriagePosition = state.carriagePosition;
          var position = {
              col: carriagePosition.col + colOffset,
              row: carriagePosition.row + rowOffset
          };
          while (position.row < state.backing.getRowOffsets().length &&
              position.row >= 0 &&
              (position.col < state.backing.getColumnOffsets().length && position.col >= 0)) {
              if (state.backing.getCellDimensions(position.col, position.row).selectable) {
                  return position;
              }
              else {
                  position.col += colOffset ? Math.sign(colOffset) : 0;
                  position.row += rowOffset ? Math.sign(rowOffset) : 0;
              }
          }
          return state.carriagePosition;
      };
      VirTable.prototype.getTopContainerHeight = function () {
          if (this.scrollTopContainer && this.scrollTopContainer.lastChild) {
              var lastChild = this.scrollTopContainer.lastChild;
              // @ts-ignore
              return lastChild.offsetTop + lastChild.clientHeight;
          }
          else {
              return 0;
          }
      };
      VirTable.prototype.getLeftContainerWidth = function () {
          if (this.scrollLeftContainer && this.scrollLeftContainer.lastChild) {
              var lastChild = this.scrollLeftContainer.lastChild;
              // @ts-ignore
              return lastChild.offsetLeft + lastChild.clientWidth;
          }
          else {
              return 0;
          }
      };
      VirTable.prototype.getFixedContainerWidth = function () {
          if (this.scrollFixedContainer && this.scrollFixedContainer.lastChild) {
              var lastChild = this.scrollFixedContainer.lastChild;
              // @ts-ignore
              return lastChild.offsetLeft + lastChild.clientWidth;
          }
          else {
              return 0;
          }
      };
      VirTable.prototype.getFixedContainerHeight = function () {
          if (this.scrollFixedContainer && this.scrollFixedContainer.lastChild) {
              var lastChild = this.scrollFixedContainer.lastChild;
              // @ts-ignore
              return lastChild.offsetTop + lastChild.clientHeight;
          }
          else {
              return 0;
          }
      };
      VirTable.prototype.scrollToCarriage = function () {
          var carriagePosition = this.state.carriagePosition;
          if (this.rootContainer) {
              var cellDimensions = this.state.backing.getCellDimensions(carriagePosition.col, carriagePosition.row);
              if (!cellDimensions.stuck) {
                  var cellPosition = __assign({}, cellDimensions.offset, { right: cellDimensions.offset.left + cellDimensions.size.width, bottom: cellDimensions.offset.top + cellDimensions.size.height });
                  var rootContainer = this.rootContainer;
                  var leftOffset = this.getLeftContainerWidth();
                  if (cellPosition.right > rootContainer.scrollLeft + rootContainer.clientWidth) {
                      rootContainer.scrollLeft = cellPosition.right - rootContainer.clientWidth;
                  }
                  else if (cellPosition.left < rootContainer.scrollLeft + leftOffset) {
                      this.rootContainer.scrollLeft = cellPosition.left - leftOffset;
                  }
                  var topOffset = this.getTopContainerHeight();
                  if (cellPosition.bottom > rootContainer.scrollTop + rootContainer.clientHeight) {
                      rootContainer.scrollTop = cellPosition.bottom - rootContainer.clientHeight;
                  }
                  else if (cellPosition.top < rootContainer.scrollTop + topOffset) {
                      rootContainer.scrollTop = cellPosition.top - topOffset;
                  }
                  this.updateArea();
              }
          }
      };
      VirTable.prototype.shiftCarriage = function (col, row) {
          var _this = this;
          this.setState(function (state) { return (__assign({}, state, { carriagePosition: _this.getAvailableCell(state, col, row) })); });
      };
      VirTable.prototype.moveCarriageToMousePosition = function (x, y) {
          var backing = this.state.backing;
          if (this.rootContainer) {
              var rootContainer = this.rootContainer;
              var widthOffset = x + (x > this.getLeftContainerWidth() && x > this.getFixedContainerWidth() ? rootContainer.scrollLeft : 0);
              var heightOffset = y + (y > this.getTopContainerHeight() && y > this.getFixedContainerHeight() ? rootContainer.scrollTop : 0);
              var col_1 = backing.getColumnAtOffset(widthOffset);
              var row_1 = backing.getRowAtOffset(heightOffset);
              if (this.state.backing.getCellDimensions(col_1, row_1).selectable) {
                  this.setState(function (state) { return (__assign({}, state, { carriagePosition: { row: row_1, col: col_1 } })); });
              }
          }
      };
      VirTable.prototype.handleKeyDown = function (event) {
          switch (event.which) {
              case KEYS.LEFT_ARROW:
                  event.preventDefault();
                  this.shiftCarriage(-1, 0);
                  break;
              case KEYS.RIGHT_ARROW:
              case KEYS.TAB:
                  event.preventDefault();
                  this.shiftCarriage(1, 0);
                  break;
              case KEYS.UP_ARROW:
                  event.preventDefault();
                  this.shiftCarriage(0, -1);
                  break;
              case KEYS.ENTER:
              case KEYS.DOWN_ARROW:
                  event.preventDefault();
                  this.shiftCarriage(0, 1);
                  break;
          }
      };
      VirTable.prototype.handleClick = function (event) {
          if (this.rootContainer) {
              var _a = this.rootContainer.getBoundingClientRect(), left = _a.left, top_1 = _a.top;
              var _b = this.rootContainer, scrollLeft = _b.scrollLeft, scrollTop = _b.scrollTop;
              var xOffset = event.clientX - left + scrollLeft;
              var yOffset = event.clientY - top_1 + scrollTop;
              if (xOffset < this.state.backing.width && yOffset < this.state.backing.height) {
                  this.moveCarriageToMousePosition(event.clientX - left, event.clientY - top_1);
                  event.preventDefault();
              }
          }
      };
      VirTable.prototype.componentDidUpdate = function (_, prevState) {
          if (prevState.carriagePosition.row !== this.state.carriagePosition.row ||
              prevState.carriagePosition.col !== this.state.carriagePosition.col) {
              this.scrollToCarriage();
          }
          if (prevState.backing !== this.state.backing && this.rootContainer) {
              this.updateArea();
          }
      };
      VirTable.prototype.componentDidMount = function () {
          if (this.rootContainer) {
              this.updateArea();
          }
      };
      VirTable.prototype.render = function () {
          var _a = this.props, style = _a.style, className = _a.className;
          var _b = this.state, backing = _b.backing, renderArea = _b.renderArea, carriagePosition = _b.carriagePosition;
          var allCells = backing.getCells(renderArea.fromWidth, renderArea.toWidth, renderArea.fromHeight, renderArea.toHeight);
          var leftCells = [];
          var topCells = [];
          var fixedCells = [];
          var otherCells = [];
          for (var _i = 0, allCells_1 = allCells; _i < allCells_1.length; _i++) {
              var cell = allCells_1[_i];
              var inCarriageRow = cell.row === carriagePosition.row;
              var inCarriageColumn = cell.col === carriagePosition.col;
              var cellRender = (React.createElement(VirCell, { top: cell.offset.top, left: cell.offset.left, key: cell.col + "/" + cell.row, width: cell.size.width, height: cell.size.height, renderer: backing.getCellRenderer(cell.col, cell.row), selection: {
                      inCarriageRow: inCarriageRow,
                      inCarriageColumn: inCarriageColumn,
                      underCarriage: inCarriageColumn && inCarriageRow,
                      selected: false
                  } }));
              if (!!cell.stuck && cell.stuck === exports.Stuck.LEFT) {
                  leftCells.push(cellRender);
              }
              else if (!!cell.stuck && cell.stuck === exports.Stuck.TOP) {
                  topCells.push(cellRender);
              }
              else if (!!cell.stuck && cell.stuck === exports.Stuck.FIXED) {
                  fixedCells.push(cellRender);
              }
              else {
                  otherCells.push(cellRender);
              }
          }
          return (React.createElement("div", { tabIndex: 0, onKeyDown: this.handleKeyDown, onClick: this.handleClick, ref: this.handleRootRef, onScroll: this.updateArea, style: style, className: virTable + " " + (className || "") }, !!renderArea.toWidth && (React.createElement("div", { style: this.getBodyDimensions(backing.width, backing.height) },
              !!leftCells.length && (React.createElement("div", { ref: this.updateScrollLeft, className: leftTable + " " + (className || "") }, leftCells)),
              !!topCells.length && (React.createElement("div", { ref: this.updateScrollTop, className: topTable + " " + (className || "") }, topCells)),
              !!fixedCells.length && (React.createElement("div", { ref: this.updateScrollFixed, className: fixedTable + " " + (className || "") }, fixedCells)),
              otherCells))));
      };
      VirTable.getDerivedStateFromProps = function (props, state) {
          var backing = state.backing.setModel(props.model);
          var renderArea = state.renderArea;
          if (backing === state.backing) {
              return state;
          }
          if (renderArea.toHeight > backing.height) {
              renderArea.fromHeight = Math.max(0, backing.height + renderArea.fromHeight - renderArea.toHeight);
              renderArea.toHeight = backing.height;
          }
          if (renderArea.toWidth > backing.width) {
              renderArea.fromWidth = Math.max(0, backing.width + renderArea.fromWidth - renderArea.toWidth);
              renderArea.toWidth = backing.width;
          }
          return __assign({}, state, { renderArea: renderArea,
              backing: backing });
      };
      return VirTable;
  }(React.Component));

  exports.VirTable = VirTable;

  Object.defineProperty(exports, '__esModule', { value: true });

}));
//# sourceMappingURL=index.js.map
