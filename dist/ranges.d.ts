import { ReactElement } from "react";
export interface CellRendererProps {
    inCarriageRow: boolean;
    inCarriageColumn: boolean;
    selected: boolean;
    underCarriage: boolean;
}
export interface RangeDimensions {
    readonly cols: number;
    readonly rows: number;
}
export declare type RangeKey = string | number;
export declare enum Stuck {
    TOP = "top",
    LEFT = "left",
    FIXED = "fixed"
}
export interface CellRange {
    readonly key: RangeKey;
    readonly dimensions?: RangeDimensions;
    readonly stuck?: Stuck;
    readonly selectable?: boolean;
}
export interface Cell extends CellRange {
    readonly type: "cell";
    readonly dimensions: RangeDimensions;
    render(selection: CellRendererProps): ReactElement<void>;
}
export interface VerticalRange extends CellRange {
    readonly type: "vertical";
    getChildren(): RangeContent[];
}
export interface HorizontalRange extends CellRange {
    readonly type: "horizontal";
    getChildren(): RangeContent[];
}
export declare type RangeContent = VerticalRange | HorizontalRange | Cell;
export interface CellGroup extends CellRange {
    readonly type: "cellGroup";
    getChildren(): GroupContent[];
}
export declare type GroupContent = RangeContent | CellGroup;
export interface TableModel {
    readonly colWidths: number[];
    readonly rowHeights: number[];
    readonly content: GroupContent;
}
