import { TableBacking } from "../../backing";
interface Position {
    row: number;
    col: number;
}
export interface RenderArea {
    fromWidth: number;
    toWidth: number;
    fromHeight: number;
    toHeight: number;
}
export interface VirTableState {
    backing: TableBacking;
    carriagePosition: Position;
    renderArea: RenderArea;
}
export {};
