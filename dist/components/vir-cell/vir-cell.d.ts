import * as React from "react";
import { VirCellProps } from "./vir-cell-props";
export declare class VirCell extends React.PureComponent<VirCellProps> {
    private getStyle;
    render(): React.ReactElement;
}
