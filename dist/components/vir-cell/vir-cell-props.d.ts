import { CellOffset, CellRenderer, CellSize } from "../../backing";
import { CellRendererProps } from "../../ranges";
export interface VirCellProps extends CellSize, CellOffset {
    renderer: CellRenderer;
    selection: CellRendererProps;
}
