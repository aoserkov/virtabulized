import { ReactElement } from "react";
import { CellRendererProps, Stuck, TableModel } from "../ranges";
export interface CellOffset {
    top: number;
    left: number;
}
export interface CellSize {
    width: number;
    height: number;
}
export interface CellGeometry extends CellOffset {
    right: number;
    bottom: number;
}
export interface CellDimensions {
    col: number;
    row: number;
    offset: CellOffset;
    size: CellSize;
    empty: boolean;
    stuck?: Stuck;
    selectable?: boolean;
}
export interface CellRenderer {
    render(props: CellRendererProps): ReactElement;
}
export declare class TableBacking {
    private readonly _model;
    private readonly _rowOffsets;
    private readonly _columnOffsets;
    private readonly _rangeTree;
    private constructor();
    readonly width: number;
    readonly height: number;
    getColumnAtOffset(widthOffset: number): number;
    getRowAtOffset(heightOffset: number): number;
    getRowOffsets(): number[];
    getColumnOffsets(): number[];
    private checkRange;
    private calculateCellDimensions;
    getCellDimensions(col: number, row: number): CellDimensions;
    getCellRenderer(col: number, row: number): CellRenderer;
    getCells(fromWidthOffset: number, toWidthOffset: number, fromHeightOffset: number, toHeightOffset: number): CellDimensions[];
    setModel(model: TableModel): TableBacking;
    static createBacking(model: TableModel): TableBacking;
}
