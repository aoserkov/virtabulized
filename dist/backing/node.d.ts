import { Cell, GroupContent, RangeDimensions, RangeKey, Stuck } from "../ranges";
export interface RangePosition {
    col: number;
    row: number;
}
export interface CellRequestResult {
    cell: Cell | null;
    position: RangePosition;
    dimensions: RangeDimensions;
    selectable?: boolean;
    stuck?: Stuck;
}
export declare function pad(toCol: number, toRow: number, innerDimensions: RangeDimensions, enclosingDimensions: RangeDimensions, consumer: (r: CellRequestResult) => boolean): boolean;
export declare function assertDimensionsFit(dimensions: RangeDimensions, constraints: RangeDimensions, key: RangeKey): void;
export interface Node {
    readonly dimensions: RangeDimensions;
    readonly stuck?: Stuck;
    readonly selectable?: boolean;
    findCellsInRange(fromCol: number, toCol: number, fromRow: number, toRow: number, enclosingDimensions: RangeDimensions, consumer: (r: CellRequestResult) => boolean, stuck?: Stuck, selectable?: boolean): boolean;
    setRange(range: GroupContent, enclosingDimensions?: RangeDimensions): Node;
}
export declare function createNode(range: GroupContent, enclosingDimensions: RangeDimensions): Node;
