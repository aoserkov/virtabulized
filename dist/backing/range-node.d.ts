import { GroupContent, RangeDimensions, Stuck } from "../ranges";
import { CellRequestResult, Node } from "./node";
interface CellRangeCache {
    [key: string]: Node;
    [key: number]: Node;
}
export declare class RangeNode implements Node {
    private readonly _range;
    private _dimensions?;
    private _order;
    private _cache;
    constructor(range: GroupContent, cache: CellRangeCache, dimensionConstraints?: RangeDimensions);
    setRange(range: GroupContent, enclosingDimensions?: RangeDimensions): Node;
    readonly dimensions: RangeDimensions;
    readonly stuck: Stuck | undefined;
    readonly selectable: boolean | undefined;
    findCellsInRange(fromCol: number, toCol: number, fromRow: number, toRow: number, enclosingDimensions: RangeDimensions, consumer: (r: CellRequestResult) => boolean, stuck?: Stuck, selectable?: boolean): boolean;
    private calculateDimensions;
    private fillCache;
}
export {};
