import { Cell, GroupContent, RangeDimensions, Stuck } from "../ranges";
import { CellRequestResult, Node } from "./node";
export declare class CellNode implements Node {
    readonly _cell: Cell;
    readonly _dimensions: RangeDimensions;
    constructor(cell: Cell, enclosingDimensions?: RangeDimensions);
    readonly dimensions: RangeDimensions;
    readonly stuck: Stuck | undefined;
    readonly selectable: boolean | undefined;
    findCellsInRange(fromCol: number, toCol: number, fromRow: number, toRow: number, enclosingDimensions: RangeDimensions, consumer: (r: CellRequestResult) => boolean, stuck?: Stuck, selectable?: boolean): boolean;
    setRange(range: GroupContent, enclosingDimensions?: RangeDimensions): Node;
}
