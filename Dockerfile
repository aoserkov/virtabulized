FROM node:12.7-alpine

RUN mkdir /var/lib/virtabulized

WORKDIR /var/lib/virtabulized

COPY package.json package-lock.json /var/lib/virtabulized/

RUN npm i

COPY rollup.config.js tsconfig.json tslint.json .prettierrc /var/lib/virtabulized/

COPY typings /var/lib/virtabulized/typings
COPY example /var/lib/virtabulized/example
COPY lib /var/lib/virtabulized/lib

RUN npm run lint:check

RUN npm run format:check

RUN npm run test

RUN npm run build

CMD npm publish
