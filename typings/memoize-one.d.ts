declare module "memoize-one" {

  export default function memoizeOne<R>(f: (...args: any[]) => R): (...args: any[]) => R;

}
