import * as React from "react";
import { render } from "react-dom";
import { CellRendererProps, RangeContent, Stuck, TableModel, VirTable } from "../../lib";

const simpleModel: TableModel = {
  colWidths: new Array(1000).fill(null).map(() => 100),
  rowHeights: new Array(1000).fill(null).map(() => 100),
  content: {
    type: "vertical",
    key: "root",
    getChildren(): RangeContent[] {
      return new Array(1000).fill(null).map((_, i) => ({
        key: `${i}`,
        type: "horizontal",
        stuck: i === 0 || i === 1 ? Stuck.TOP : undefined,
        getChildren(): RangeContent[] {
          return new Array(1000).fill(null).map((__, j) => {
            const selectable: boolean = i === 0 || i === 1 || j === 0 ? false : true;

            return {
              key: `${j}`,
              type: "cell",
              stuck: j === 0 ? Stuck.LEFT : undefined,
              selectable,
              dimensions: { cols: 1, rows: 1 },
              render(selection: CellRendererProps): React.ReactElement<void> {
                const backgroundColor: string =
                  selection.underCarriage ? "green" :
                  !selectable ? "gray" :
                  selection.inCarriageRow || selection.inCarriageColumn ? "blue" : "white"

                if (i === 0 && j === 0) {
                  return <>Empty</>;
                }

                return (
                  <div style={{ backgroundColor, width: "100%", height: "100%" }}>
                    {i}/{j}
                    {selection.underCarriage ? "C" :
                    selection.inCarriageColumn ? "|" :
                    selection.inCarriageRow ? "-" : ""}
                  </div>
                );
              }
            }
          })
        }
      }));
    }
  }
};

render(<VirTable model={simpleModel} style={{ height: "100%", width: "100%" }} />, document.getElementById("root"));
